.code16
.globl		read
.type		read, @function
read:
		pusha
		# reads the string
		movb	$0x0, %ah
		int	$0x16
		# echos the pressed string
		movb    $0x0E, %ah
		int     $0x10
		popa
		ret
