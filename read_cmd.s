.code16
.globl		read_cmd
.type		read_cmd, @function
read_cmd:
new_cmd:	.byte	'>'
		pusha
		movb	new_cmd, %al
		movb	$0x0E, %ah
		int	$0x10

		# reads the command
		movb	$0x0, %ah
		int	$0x16

		# echos the pressed key
		movb	$0x0E, %ah
		int     $0x10

		# selects the command
		cmp	$'1', %al
		je	do_reboot

		cmp	$'2', %al
		je	do_clean

		cmp	$'3', %al
		je	do_print

		cmp	$'4', %al
		je	do_check

		cmp	$'5', %al
		je	do_memory

		jmp	print_error

do_reboot:	call	reboot

do_clean:	call	clrscreen
		jmp	end

do_print:
		mov	$msg, %bx
		call	printstr
		jmp	end

do_check:	call	look_equipment
		jmp	end

do_memory:	call	memory_size
		jmp	end

print_error:	mov	$error, %bx
		call	printstr

end:		popa
		ret
