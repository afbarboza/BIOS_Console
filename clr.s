.code16
.globl		clrscreen
.type		clrscreen, @function
clrscreen:
		movb	$0x0, %al
		movb	$0x0, %ah
		int	$0x10
		ret
