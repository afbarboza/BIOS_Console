.code16
.globl		memory_size
.type		memory_size, @function
memory_size:
		pusha

		movw	$newline, %bx
		call	printstr

		movw	$0x0, %ax

		# getting fourth nibble
		int	$0x12		# query memory size
		shrw	$12, %ax
		andw	$0xF, %ax
		call	conv_hex_ascii

		# getting third nibble
		int	$0x12		# query memory size
		shrw	$8, %ax
		andw	$0xF, %ax
		call	conv_hex_ascii

		# getting second nibble
		int	$0x12		# query memory size
		shrw	$4, %ax
		andw	$0xF, %ax
		call	conv_hex_ascii

		# getting first nibble
		int	$0x12		# query memory size
		andw	$0xF, %ax
		call	conv_hex_ascii

		movw	$newline, %bx
		call	printstr

		popa
		ret

/**
 * conv_hex_ascii: converts a hex value
 *	to ascii.
 *
 * parameters:
 *		ax: the value to be converted and printed.
 */
.globl		memory_size
.type		conv_hex_ascii, @function
conv_hex_ascii:
		pusha
		addb	$0x30, %al
		cmpb	$0x39, %al
		jg	conv_jg
		jmp	conv_end	
conv_jg:	addb	$0x7, %al
conv_end:	movb    $0x0E, %ah
		int	$0x10
		popa
		ret
