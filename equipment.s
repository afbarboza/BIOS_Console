.code16
.globl		look_equipment
.type		look_equipment, @function
look_equipment:
		jmp	lookup_devices

/**
 * global variables used in this code
 */
# bitnn_found - string to be displayed if the specified bit is set
bitFF_found:	.ascii	"\r\n-->Keyboard found.\r\n\0"
bitFF_notfound:	.ascii	"\r\n-->Keyboard not found.\r\n\0"

bit00_found:	.ascii	"-->Floppy drive found.\r\n\0"
bit00_notfound:	.ascii	"-->Floppy drive not found.\r\n\0"

bit54_found:	.ascii	"-->80x25 color screen found.\r\n\0"
bit54_notfound:	.ascii	"-->80x25 color screen not found.\r\n\0"

lookup_devices:
		pusha
		xorw	%ax, %ax
		xorw	%bx, %bx
# checking whether keyboard is present
check_keyboard:
		movb	$0x0, %al
		movb	$0xA, %ah
		int	$0x16
		cmpw	$0x0, %bx	
		je	.L2
		movw	$bitFF_found, %bx
		jmp	.L3
.L2:		movw	$bitFF_notfound, %bx
.L3:		call	printstr

# ask BIOS for installed devices
		xorw	%ax, %ax
		int	$0x11
#here:		jmp	here

# checking for floppy drive
check_floppy:
		xorw	%bx, %bx
		movw	%ax, %bx
		andw	$0x01, %bx
		cmpw	$0x01, %bx # bx = 0, no floppy found.
		jne	.L4
		movw	$bit00_found, %bx
		jmp	.L5
.L4:		movw	$bit00_notfound, %bx
.L5:		call	printstr

# checking for color screen
check_screen:
		xorw	%bx, %bx
		movw	%ax, %bx
		andw	$0x20, %bx
		cmpw	$0x20, %bx
		jne	.L6
		movw	$bit54_found, %bx
		jmp	.L7
.L6:		movw	$bit54_notfound, %bx
.L7:		call	printstr

end_check:
		popa
		ret
