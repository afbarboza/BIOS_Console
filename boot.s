
.section	.text
.type		_start, @function
.globl		_start
_start:
.code16
		jmp	main
# pseudo-area of data
msg:		.ascii "\r\nBootloader v0.1\r\n\0"
error:		.ascii "\r\nThere is no such program\r\n\0"
newline:	.ascii "\r\n\0"

main:
	xorw	%ax, %ax
	xorw	%bx, %bx


loop:	call	new_cmd
	jmp	loop

.include "read_cmd.s"
.include "print.s"
.include "read.s"
.include "clr.s"
.include "reboot.s"
.include "equipment.s"
.include "memory.s"

. = _start + 510
	.byte	0x55, 0xAA
